from django import forms

class CalcForms(forms.Form):
    first = forms.IntegerField(label='first number', max_value=1000, min_value=-1000, required=True)
    second = forms.IntegerField(label='second number', max_value=1000, min_value=-1000, required=True)

class RegForms(forms.Form):
    name = forms.CharField(label='Your name', required=True)
    password = forms.CharField(label='Your password', required=True, widget=forms.PasswordInput)