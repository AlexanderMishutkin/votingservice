from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class CalcHistory(models.Model):
    date = models.DateTimeField()
    first = models.IntegerField()
    second = models.IntegerField()
    result = models.IntegerField()
    author = models.ForeignKey(to=User,on_delete=models.CASCADE, null=True)