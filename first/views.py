from django.shortcuts import render
from django import http
from django.contrib.auth.decorators import login_required
from first import forms
import datetime
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login


# Create your views here.
#@login_required
def index(request):
    return http.httpResponce('kek')


def registration(request):
    context = {}
    if request.method == 'POST':
        f = forms.RegForms(request.POST)
        if f.is_valid():
            try:
                curr_user = User.objects.create_user(f.data['name'], '??', f.data['password'])
            except:
                context['form'] = forms.RegForms()
                context['errors'] = 'user already exist'
                return render(request, 'register.html', context)
            if (curr_user != None):
                login(request, curr_user)
            else:
                context['form'] = forms.RegForms()
                context['errors'] = 'user already exist'
                return render(request, 'register.html', context)
            return http.HttpResponseRedirect('/')
    else:
        context['form'] = forms.RegForms()
    return render(request, 'register.html', context)